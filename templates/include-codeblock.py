#!/usr/bin/env python3
# coding: utf-8

"""
Made by Hervé Frezza-Buet
It expands a file into .md.
See tuto.
"""

from pandocfilters import toJSONFilter, CodeBlock, Str

def expand_include(key, value, format, meta):
    if key == 'CodeBlock':
        title, args, values = value[0]
        if 'include' not in [k for k,v in values]:
            return None
        vs = []
        for k,v in values :
            if k == 'include' :
                with open(v,'r') as file:
                    code = file.read()
            else:
                vs.append((k, v))
        return CodeBlock([title, args, vs], code)
    
if __name__ == "__main__":
    toJSONFilter(expand_include)
