---
title:  Première partie - MISE EN ROUTE
layout: parc
---

??? info "Vidéos $\blacksquare$ Premiers pas"


    > [Vidéo $\blacksquare$ Premiers pas - partie 1 - Lance-toi !](http://www.yout-ube.com/watch?v=9M9yz8bFzuU)

    > [Vidéo $\blacksquare$ Premiers pas - partie 2 - Variable](http://www.yout-ube.com/watch?v=9h9nLLRRjls)

    > [Vidéo $\blacksquare$ Premiers pas - partie 3 - Fonctions](http://www.yout-ube.com/watch?v=WLTNpIdHIls)

    > [Vidéo $\blacksquare$ Premiers pas - partie 4 - Boucle pour](http://www.yout-ube.com/watch?v=zxf7QRfZato)

    > [Vidéo $\blacksquare$ Installer Python](http://www.yout-ube.com/watch?v=IS117_uXZKE)

    > [Vidéo $\blacksquare$ Démarrer Python et utiliser IDLE](http://www.yout-ube.com/watch?v=QtUxL52VPtA)

# Premiers pas

Lance-toi dans la programmation ! 

!!! warning "Suivre les activités en ligne ci-dessous"
    
!!! abstract "Dans cette toute première activité"
    Tu vas apprendre à manipuler des nombres, des variables et tu vas coder tes premières boucles avec `Python`.  
    *-> pour consulter le tout en pdf : [2nde_python1.pdf](./2nde_python1.pdf)*


    [Cours 1 - Nombres avec `Python`](premierspas/cours1.md){ .md-button }

    [Cours 2 - Travailler avec un éditeur](premierspas/cours2.md){ .md-button }

    [Cours 3 - Variables](premierspas/cours3.md){ .md-button }

    [Cours 4 - Utiliser des fonctions](premierspas/cours4.md){ .md-button }

    [Cours 5 - Boucle pour](premierspas/cours5.md){ .md-button }
