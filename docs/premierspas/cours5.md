---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 5 - Boucle pour

![Figure boucle pour](../im/fig-premiers_pas-boucle-pour.png){ align=center }

<!---    
\usetikzlibrary{positioning}
\begin{tikzpicture}[scale=1, every node/.style={transform shape}]
\Large
\node[right] (A) at (0,0) {\texttt{for}};
\node[right,right=-0.5em of A] (B) {\texttt{i}};
\node[right,right=-0.5em of B] (C)  {\texttt{in}};
\node[right,right=-0.5em of C] (D) {\texttt{range(n)}};
\node[right,right=-1.4em of D] (EE) {\texttt{:}};



 \draw[<-,>=latex,ultra thick, red!20]    (0.7,-1.6)  to[bend right=10] (9,-3.2) node[red,right] {indentation};


\node[right] (E) at (1,-0.7) {\texttt{instruction\_1}};
\node[right] at (1,-1.4) {\texttt{instruction\_2}};
\node[right] at (1,-2.1) {\texttt{...}};
\node[right] (F) at (0,-2.8) {\texttt{instructions suivantes}};

 \draw[<-,>=latex,ultra thick, red]  (A.north) to[bend left] (9,3) node[right] {mots r\'eserv\'es "\texttt{for}" et "\texttt{in}"};
 \draw[<-,>=latex,ultra thick, red!20]  (C.north) to[bend left] (9,3);
 \draw[<-,>=latex,ultra thick, red]  (B.north) to[bend left] (9,2.2) node[right] {une variable};
 \draw[<-,>=latex,ultra thick, red]  (D.north) to[bend left] (9,1.4) node[right] {liste \`a parcourir, ici $0$, $1$, $2$, \ldots, $n-1$};
 \draw[<-,>=latex,ultra thick, red]  (EE.north) to[bend left] (9,0.6) node[right] {deux points};

 \draw[|-|,ultra thick, red] (5.5,-0.3)--++(0,-2.1);
 \draw[ultra thick, red]  (5.5,-1.2) to[bend right] (10,-0.5) node[right] {bloc d'instructions indent\'e};
 \node[right,red]  at (10,-1.2) {sera r\'ep\'et\'e $n$ fois};
 \node[right,red]  at (10,-1.9) {pour $i=0$, puis $i=1$\ldots{} jusqu'\`a $i=n-1$};

 \draw[<-,>=latex,ultra thick, red]    (F.south)  to[bend right=15] (9,-4.2) node[right] {suite du programme};

\node[blue,right,scale=0.8] at (0.08,-0.85) {\textvisiblespace\textvisiblespace\textvisiblespace\textvisiblespace};

\node[blue,right,scale=0.8] at (0.08,-1.55) {\textvisiblespace\textvisiblespace\textvisiblespace\textvisiblespace};

\node[blue,right,scale=0.8] at (0.08,-2.2) {\textvisiblespace\textvisiblespace\textvisiblespace\textvisiblespace};


\end{tikzpicture}
-->

!!! tip "La boucle pour est la façon la plus simple de répéter des instructions"
    Note bien que ce qui délimite le bloc d'instructions à répéter, c'est **l'indentation**,  
    c'est-à-dire les espaces placées en début de ligne qui décalent les lignes vers la droite.  
    Toutes les lignes d'un bloc doivent avoir exactement la même indentation.

!!! example "Exemple de boucle pour"
    Voici une boucle qui affiche les premiers carrés.


{{ py('pour5') }}
<!---
```py3
for i in range(10):
    print(i*i)
```
-->

!!! danger "N'oublie pas les deux points en fin de la ligne de la déclaration du `for` !"
    Nous choisissons une indentation de $4$ espaces.  
    
    **La seconde ligne est décalée et constitue le bloc à répéter**  
    La variable `i` prend la valeur $0$ et l'instruction affiche $0^2$ ;  
    puis `i` prend la valeur $1$, et l'instruction affiche $1^2$ ; puis $2^2$, $3^2$...*

    *Au final ce programme affiche :*
    
    $$0,1,4,9,16,25,36,49,64,81.$$

!!! cite "la dernière valeur prise par `i` est bien $9$ (et pas $10$)"

{{ basthon('scripts/pour5.py', 800) }}
<!---<iframe src="https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/pour5.py" width=100% height=800></iframe>
[Lien dans une autre page](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/pour5.py)-->
<!---{{ basth('scripts/pour5.py', 800) }}-->

!!! note "Parcourir une liste quelconque"
    La boucle pour permet de parcourir n'importe quelle liste.  
    Voici une boucle qui affiche le cube des premiers nombres premiers.

{{ py('prem5') }}
<!---
```py3
for i in range(10):
    print(i*i)
```
-->


!!! note "Sommes des entiers"
    Voici un programme qui calcule   $0+1+2+3+\cdots + 18 +19.$

{{ py('som5') }}
<!---
```py3
somme = 0
for i in range(20):
    somme = somme + i

print(somme)
```
-->


!!! info "Comprends bien ce code"
    Une variable `somme` est initialisée à $0$. On va tour à tour lui ajouter $0$, puis $1$, puis $2$...  
    On peut mieux comprendre cette boucle en complétant un tableau :  
    
    $$\begin{array}{l}
      \text{Initialisation : somme$=0$}    \\
      \begin{array}{|c|c|}
      \hline  
       \text{i} &  \text{somme} \\
      \hline\hline
      0 & 0 \\  
      1 & 1 \\
      2 & 3 \\
      3 & 6 \\
      4 & 10\\
      \ldots & \ldots \\
      18 & 171 \\
      19 & 190 \\ 
      \hline
      \end{array} \\
      \text{Affichage : $190$}  
    \end{array}$$



!!! note "Imbrication de boucles"
    Il est possible d'imbriquer des boucles, c'est-à-dire que dans le bloc d'une boucle, on utilise une nouvelle boucle.

{{ py('bimb5') }}
<!---
```py3
for x in [10,20,30,40,50]:
    for y in [3,7]:
        print(x+y)
```
-->

!!! info "Dans ce petit programme"
    $x$ vaut d'abord $10$, $y$ prend la valeur $3$ puis la valeur $7$ (le programme affiche donc $13$, puis $17$).  
    Ensuite $x=20$, et $y$ vaut de nouveau $3$ puis $7$ (le programme affiche donc ensuite $23$, puis $27$).  
    Au final le programme affiche : 
    
    $$13,17,23,27,33,37,43,47,53,57.$$


??? faq "Activité 1.4 (Boucle pour)"
    1. 
        (a) *Affiche les cubes des entiers de $0$ à $100$.*

    
        (b) *Affiche les puissances quatrièmes des entiers de $10$ à $20$.*

    
        (c) *Affiche les racines carrées des entiers $0$, $5$, $10$, $15$,... jusqu'à $100$.*
        
    2. *Affiche les puissances de $2$, de $2^1$ à $2^{10}$, et apprends par cœur les résultats !*


    3. *Recherche de façon expérimentale une valeur approchée du minimum de la fonction sur l'intervalle $[0,1]$ : $f(x) = x^3-x^2-\frac14x+1$*
    
        ??? abstract "Indications."
        -   *Construis une boucle dans laquelle une variable $i$ balaye les entiers de $0$ à $100$.*
        -   *Définis $x=\frac{i}{100}$. Ainsi $x=0.00$, puis $x=0.01$, $x=0.02$...*
        -   *Calcule $y = x^3-x^2-\frac14x+1$.*
        -   *Affiche les valeurs à l'aide de `print("x =",x,"y =",y)`.*
        -   *Cherche à la main pour quelle valeur de $x$ on obtient un $y$ le plus petit possible.*
        -   *N'hésite pas à modifier ton programme pour augmenter la précision.*
        
        

    4.  *Cherche une valeur approchée que doit avoir le rayon $R$ d'une boule afin que son volume soit $100$.*
    
        ??? abstract "Indications."
        -   *Utilise une méthode de balayage comme à la question précédente.*
        -   *La formule du volume d'une boule est $V = \frac43 \pi R^3$.*
        -   *Affiche les valeurs à l'aide de `print("R =",R,"V =",V)`.*
        -   *Pour $\pi$ tu peux prendre la valeur approchée $3.14$ ou bien la valeur approchée `pi` du module `math`.*

!!! tip "Objectifs : construire des boucles simples."


??? faq "Activité 1.5 (Boucle pour (suite))"
    1. *Définis une variable $n$ (par exemple $n=20$). Calcule la somme  $1^2+2^2+3^2+\cdots+i^2+\cdots +n^2.$*

    2.  *Calcule le produit :   $1 \times 3 \times 5 \times \cdots \times 19.$*

        ??? abstract "Indications."
        *Commence par définir une variable `produit` initialisée à la valeur $1$.  
        Utilise `range(a,b,2)` pour obtenir un entier sur deux.*

    3.  *Affiche les tables de multiplication entre $1$ et $10$. Voici un exemple de ligne à afficher : `7 x 9 = 63`*

        ??? abstract "Indications."
        *Utilise une commande d'affichage du style : `print(a,"x",b,"=",a*b)`.*


!!! tip "Objectifs : construire des boucles plus compliquées."

