---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 2 - Travailler avec un éditeur

![image](../im/editeur.png){ align=right width=400 }

!!! done "Éditeur de texte dédié à `Python`"
    À partir de maintenant, il est préférable que tu travailles dans un éditeur.  
    

    ??? abstract "Tu dois alors explicitement demander d'afficher le résultat :"
    	La commande `print(2+2)` affiche sur la sortie standard l'argument passé entre parenthèses et un retour à la ligne.
        
        
{{ py('editeur0') }}

{{ bast(800) }}

