---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 3 - Variables

!!! tip "Une variable est un nom associé à un emplacement de la mémoire"
    C'est comme une boîte que l'on identifie par une étiquette. 

    ??? abstract "a = 3"
        La commande `a = 3` signifie que j'ai une variable `a` associée à la valeur $3$.
        


!!! example "Voici un premier exemple :"

{{ py('exempc3') }}
<!---
```py3
a = 3    # Une variable
b = 5    # Une autre variable

print("La somme vaut",a+b)    # Affiche la somme
print("Le produit vaut",a*b)  # Affiche le produit

c = b**a    # Nouvelle variable...
print(c)    # ... qui s ' affiche
```
-->
{{ basthon('scripts/exempc3.py', 800) }}
<!---<iframe src="https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/exempc3.py" width=100% height=800></iframe>
[Lien dans une autre page](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/exempc3.py)-->
<!---{{ basth('scripts/exempc3.py', 800) }}-->

!!! cite "Commentaires"
    Tout texte qui suit le caractère dièse \# n'est pas exécuté par `Python` mais sert à expliquer le programme.  
    C'est une bonne habitude de commenter abondamment ton code.

!!! note "Noms"
    Il est très important de donner un nom clair et précis aux variables.  
    Par exemple, avec les noms bien choisis tu devrais savoir ce que calcule le code suivant :

{{ py('nomc3') }}

<!---
```py3
base = 8
hauteur = 3
aire = base * hauteur / 2
print(aire)
print(Aire)    \# !! Erreur !!*
```
-->

!!! danger "`Python` distingue les majuscules des minuscules"
    Donc `mavariable`, `Mavariable` et `MAVARIABLE` sont des variables différentes.

!!! note "Réaffectation"
    Imaginons que tu veuilles tenir tes comptes journaliers. 
    
    ??? abstract "Tu pars d'une somme $S_0 = 1000$"
        le lendemain tu gagnes $100$,donc maintenant $S_1 = S_0 + 100$ ;  
        le jour d'après tu rajoutes $200$, donc $S_2 = S_1 + 200$ ;  
        puis tu perds $50$, donc au troisième jour $S_3 = S_2 - 50$.  
        Avec `Python` tu peux n'utiliser qu'une seule variable `S` pour toutes ces opérations.

{{ py('reaffc3') }}

<!---
```py3
S = 1000
S = S + 100
S = S + 200
S = S - 50
print(S)
```
-->

!!! note "Il faut comprendre l'instruction `S = S + 100` comme ceci :"
    je prends le contenu de la boîte `S`, je rajoute $100$, je remets tout dans la même boîte.


??? faq "Activité 1.2 (Variables)"
    ![Figure trapèze](../im/fig-trapeze.png){ align=right }
    
    1. 
        (a) *Définis des variables, puis calcule l'aire d'un trapèze.  
            Ton programme doit afficher `"L'aire vaut ..."` en utilisant `print("L'aire vaut",aire)`.*  

    
        (b) *Définis des variables pour calculer le volume d'une boîte (un parallélépipède rectangle) dont les dimensions sont $10$, $8$, $3$.*

    
        (c) *Définis une variable `PI` qui vaut $3.14$.  
        Définis un rayon $R = 10$.  
        Écris la formule de l'aire du disque de rayon $R$.*
        
    2. *Remets les lignes dans l'ordre de sorte qu'à la fin $x$ ait la valeur $46$.*
      
        $$\begin{eqnarray}
         &(1) &y &=& y - 1 \\
         &(2) &y &=& 2*x \\
         &(3) &x &=& x + 3*y \\
         &(4) &x &=& 7 \\
        \end{eqnarray}$$
        
    3.  *Tu places la somme de $1000$ euros sur un compte d'épargne.  
        Chaque année les intérêts sur l'argent placé rapportent $10\%$ (le capital est donc multiplié par $1.10$).    
        Écris le code qui permet de calculer le capital pour les trois premières années.*
        
    4.  *Je définis deux variables par `a = 9` et `b = 11`. Je souhaite échanger le contenu de `a` et `b`.  
        Quelles instructions conviennent de sorte qu'à la fin `a` vaut $11$ et `b` vaut $9$ ?*
        
        === "choix 1"
            
            $$\begin{eqnarray}
            a &= b \\
            b &= a \\
            \end{eqnarray}$$
            
        === "choix 2"
            
            $$\begin{eqnarray}
            c &= b \\
            a &= b \\
            b &= c \\
            \end{eqnarray}$$
            
        === "choix 3"
            
            $$\begin{eqnarray}
            c &= a \\
            a &= c \\
            c &= b \\
            b &= c \\
            \end{eqnarray}$$          

!!! tip "Objectifs : utiliser des variables !"

<!---
```{.tikz}
\begin{tikzpicture}
\filldraw[blue,fill=blue!10,very thick] (0,0) -- node[midway, below,black]{$B=7$} ++(7,0) -- ++ (-2,3) --node[midway, above,black]{$b=4$} ++(-4,0) --cycle;
\draw (4.5,0) -- ++(0,3) node[midway,left]{$h=3$};
\end{tikzpicture}
```
-->
