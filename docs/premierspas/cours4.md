---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 4 - Utiliser des fonctions

!!! tip "Utiliser des fonctions de `Python`"
    Tu connais déjà la fonction `print()` qui affiche une chaîne de caractères (ou des nombres).  
    Elle s'utilise ainsi `print("Coucou")` ou bien à travers une valeur :

{{ py('fonc4') }}
<!---
```py3
chaine = "Bonjour"
print(chaine)
```
-->
        


!!! example "Il existe plein d'autres fonctions"
    Par exemple la fonction `abs()` calcule la valeur absolue : `abs(-3)` renvoie `3`, `abs(5)` renvoie `5`.


!!! note "Le module `math`"
    Toutes les fonctions ne sont pas directement accessibles. Elles
    sont souvent regroupées dans des **modules**.
    
    ??? abstract "Par exemple le module `math` contient les fonctions mathématiques"
        Tu y trouves par exemple la fonction racine carrée `sqrt()` (*square root*). Voici comment l'utiliser :
                
!!! example "La première ligne du script ci-dessous importe toutes les fonctions du module `math`"
    la seconde calcule $x = \sqrt{2}$ (en valeur approchée) et ensuite on affiche $x$ et $x^2$.

{{ py('sqrtc4') }}
<!---
```py3
from math import *
        
x = sqrt(2)
print(x)
print(x**2)
```
-->

{{ basthon('scripts/sqrtc4.py', 800) }}
<!---<iframe src="https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/sqrtc4.py" width=100% height=800></iframe>
[Lien dans une autre page](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Fklag/2nde_python1/main/docs/premierspas/scripts/sqrtc4.py)-->
<!---{{ basth('scripts/sqrtc4.py', 800) }}-->

!!! note "Sinus et cosinus"
    Le module `math` contient les fonctions trigonométriques sinus et
    cosinus et même la constante `pi` qui est une valeur approchée de
    $\pi$. 

    ??? abstract "Voici le calcul de $\sin(\frac\pi2)$"
        Attention, les angles sont exprimés en radians.

{{ py('sinc4') }}
<!---
```py3
angle = pi/2
print(angle)
print(sin(angle))
```
-->

!!! note "Décimal vers entier"
    Dans le module `math` 
    
    ??? abstract "il y aussi des fonctions pour arrondir un nombre décimal :"
        -   `round()` arrondit à l'entier le plus proche : `round(5.6)` renvoie `6`, `round(1.5)` renvoie `2`.

        -   `floor()` renvoie l'entier inférieur ou égal : `floor(5.6)` renvoie `5`.

        -   `ceil()` renvoie l'entier supérieur ou égal : `ceil(5.6)` renvoie `6`.


??? faq "Activité 1.3 (Utiliser des fonctions)"
    1.  *La fonction `Python` pour le pgcd est `gcd(a,b)` (sans le p, pour *greatest common divisor).*  
        *Calcule le pgcd de $a = 10,403$ et $b = 10,506$. Déduis-en le ppcm de $a$ et $b$.  
         La fonction ppcm n'existe pas, tu dois utiliser la formule :*
         
    	$$\text{ppcm}(a,b) = \frac{a \times b}{\text{pgcd}(a,b)}$$

    
    2.  *Trouve par tâtonnement un nombre réel $x$ qui vérifie toutes les conditions suivantes (plusieurs solutions sont possibles) :*
    

        -   *`abs(x**2 - 15)` est inférieur à `0.5`*  
        -   *`round(2*x)` renvoie `8`*
        -   *`floor(3*x)` renvoie `11`*
        -   *`ceil(4*x)` renvoie `16`*
        
        **Indication.** `abs()` désigne la fonction valeur absolue.

    3.  *Tu connais la formule de trigonométrie*
    
    	$$\cos^2 \theta + \sin^2 \theta = 1.$$ 
         
         *Vérifie que pour $\theta = \frac\pi7$ (ou d'autres valeurs) cette formule est numériquement vraie.  
         (Ce n'est pas une preuve de la formule, car `Python` ne fait que des calculs approchés du sinus et du cosinus).*
     

!!! tip "Objectifs : utiliser des fonctions de `Python` et du module `math`."

