---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 1 - Nombres avec `Python`

![image](../im/console.png){ align=right width=400 }

!!! done "Vérifie dans la console que `Python` fonctionne correctement"
    En tapant les commandes suivantes dans une console `Python` :  
    

```py3
>>> 2+2
4
>>> "Bonjour le monde !"
'Bonjour le monde !'
```

{{ bast(800) }}

!!! note "Voici quelques instructions" 
    - **Addition.** `5+7`. 
    -   **Multiplication.** `6*7` ; avec des parenthèses `3*(12+5)` ; avec des nombres à virgule `3*1.5`
    -   **Puissance.** `3**2` pour $3^2=9$ ; puissance négative `10**-3` pour $10^{-3} = 0.001$ 
    -   **Division réelle.** `14/4` vaut `3.5` ; `1/3` vaut `0.3333333333333333`. 
    -   **Division entière et modulo.** -   `14//4` vaut `3` :  
        c'est le quotient de la division euclidienne de $14$ par $4$, note bien la double barre ;  
        *`14%4` vaut `2` : c'est le reste de la division euclidienne de $14$ par $4$, on dit aussi $14$ modulo $4$.*

!!! cite "Remarque"
    Dans tout ce cours, on écrira les nombres à virgule sous la forme $3.5$ (et pas $3,5$).  
    Le séparateur décimal est donc le point. En informatique les nombres à virgule sont appelés nombres flottants.


??? faq "Activité 1.1 (Premiers pas)"
    1.  *Combien y a-t-il de secondes en un siècle ? (Ne tiens pas compte des années bissextiles.)*
    2.  *Jusqu'où faut-il compléter les pointillés pour obtenir un nombre plus grand qu'un milliard ?*  
            $(1+2)\times(3+4)\times(5+6)\times(7+8)\times\cdots$
    3.  *Quels sont les trois derniers chiffres de*
        $123456789 \times 123456789 \times 123456789 \times 123456789 \times 123456789 \times 123456789 \times 123456789 \quad ?$
    4.  *$7$ est le premier entier tel que son inverse a une écriture décimale périodique de longueur $6$ :*
            $\frac{1}{7} = 0.\underbrace{142857}\underbrace{142857}\underbrace{142857}\ldots$  
            *Trouve le premier entier dont l'inverse a une écriture décimale périodique de longueur $7$ :*
            $\frac{1}{???} = 0.00\underbrace{abcdefg}\underbrace{abcdefg}\ldots$ **Indication.** L'entier est plus grand que $230$ !
    5.  *Trouve l'unique entier :*  
        -   *qui donne un quotient de $107$ lorsque l'on effectue sa division (euclidienne) par $11$,*  
        -   *et qui donne un quotient de $90$ lorsque l'on effectue sa division (euclidienne) par $13$,*  
        -   *et qui donne un reste égal à $6$ modulo $7$ !*
            
!!! tip "Objectifs : faire tes premiers calculs avec `Python`"


