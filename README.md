# python au lycée

<quote>Dépôt pour le site 2nde Python au lycée Beaussier généré avec Mkdocs</quote>

 <article>
        <p>F. Lagrave - Sources Arnaud Bodin "Python au lycée" Tome 1<a href="https://github.com/exo7math/python1-exo7">python1-exo7</a>.</p>
          <p>Tous les documents sur ce site sont placés sous licence 
            <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/"> 
              Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0).</a>
              </p>              
 </article>
